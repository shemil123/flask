from flask import *
import pymysql

app=Flask(__name__)

@app.route('/')
def student():
    return render_template('index.html')

@app.route('/home')
def home():
    return render_template('index.html')

@app.route('/register_form')
def register_form():
    return render_template('student_registration.html')

@app.route('/update_form_fetch')
def update_form_fetch():
    return render_template('update_form_fetch.html',)

@app.route('/update_form',methods=['POST','GET'])
def update_form():
    if request.method=='POST':
        result=request.form
        db = pymysql.connect(host='localhost',user='shemil',passwd='shemil_pass')
        cursor = db.cursor()

        query = ("USE College_DB;")
        cursor.execute(query)

        cursor.execute('SELECT * from Students WHERE student_id=%s ;',(result['id']))
        rows = cursor.fetchall()
        print('rows[0] : ',rows[0])
        st_id = rows[0][0]
        fname = rows[0][1]
        lname = rows[0][2]
        dob = rows[0][3]
        due = rows[0][4]
        pass_dict = {"st_id":st_id,"fname":fname,"lname":lname,"dob":dob,"due":due}
        db.close()
        return render_template('update_student_registration.html',st_id=st_id, fname = fname, lname=lname, dob = dob, due=due)

@app.route('/update',methods=['POST','GET'])
def update():
    if request.method=='POST':
        result=request.form
        student_id = result['id']
        first_name = result['fname']
        last_name = result['lname']
        dob = result['dob']
        amount_due = result['due']

        db = pymysql.connect(host='localhost',user='shemil',passwd='shemil_pass')
        cursor = db.cursor()

        query = ("USE College_DB;")
        cursor.execute(query)

        cursor.execute("UPDATE College_DB.Students SET first_name=%s,last_name=%s,dob=%s,amount_due=%s WHERE student_id=%s",(first_name,last_name,dob,amount_due,student_id))
        print(cursor.rowcount)
        db.commit()
        db.close()

        return render_template("result_data.html",result=result)

@app.route('/view_student_form')
def view_student_form():
    return render_template('views_student_form.html')

@app.route('/delete_student_form')
def delete_student_form():
    return render_template('delete_student_record_form.html')

@app.route('/view_all')
def view_all():
    db = pymysql.connect(host='localhost',user='shemil',passwd='shemil_pass')
    cursor = db.cursor()

    query = ("USE College_DB;")
    cursor.execute(query)

    cursor.execute('SELECT * from Students')
    rows = cursor.fetchall()
    db.close()
    return render_template('view_all.html',result=rows)

@app.route('/view_student_details',methods=['POST','GET'])
def view_student_details():
    if request.method=='POST':
        result=request.form
        db = pymysql.connect(host='localhost',user='shemil',passwd='shemil_pass')
        cursor = db.cursor()

        query = ("USE College_DB;")
        cursor.execute(query)

        cursor.execute('SELECT * from Students WHERE student_id=%s ;',(result['id']))
        rows = cursor.fetchall()
        db.close()
        return render_template('show_student_details.html',result=rows)

@app.route('/delete_student_details',methods=['POST','GET'])
def delete_student_details():
    if request.method=='POST':
        result=request.form
        db = pymysql.connect(host='localhost',user='shemil',passwd='shemil_pass')
        cursor = db.cursor()

        query = ("USE College_DB;")
        cursor.execute(query)

        cursor.execute('DELETE from Students WHERE student_id=%s ;',(result['id']))
        db.commit()
        db.close()
        return render_template('delete_popup.html')
    

@app.route('/register',methods=['POST','GET'])
def print_data():
    if request.method=='POST':
        result=request.form
        student_id = result['id']
        first_name = result['fname']
        last_name = result['lname']
        dob = result['dob']
        amount_due = result['due']

        db = pymysql.connect(host='localhost',user='shemil',passwd='shemil_pass')
        cursor = db.cursor()

        query = ("USE College_DB;")
        cursor.execute(query)

        cursor.execute("INSERT INTO College_DB.Students (student_id,first_name,last_name,dob,amount_due) VALUES (%s,%s,%s,%s,%s);",(student_id,first_name,last_name,dob,amount_due))
        print(cursor.rowcount)
        db.commit()
        db.close()

        return render_template("result_data.html",result=result)

if __name__=='__main__':
    app.run(debug=True)